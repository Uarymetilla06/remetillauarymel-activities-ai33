import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import VueSidebarMenu from '../src/index'

import Assetsequip from './components/Assetsequip.vue'
import Settings from './components/Settings.vue'
import Suppliers from './components/Suppliers.vue'

Vue.use(VueRouter)
Vue.use(VueSidebarMenu)

const router = new VueRouter({
  routes: [
    {
      path: '/',
      name: 'Assetsequip',
      component: Assetsequip
    },
    {
      path: '/basic-usage',
      name: 'Settings',
      component: Settings
    },
    {
      path: '/props',
      name: 'Suppliers',
      component: Suppliers
    }
  ]
})

new Vue({ // eslint-disable-line no-new
  el: '#app',
  router,
  render: h => h(App)
})
